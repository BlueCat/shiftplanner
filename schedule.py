#!/usr/bin/python
# coding=utf-8

import sys
import os
import copy
import datetime
import getopt
import markup
import codecs
import copy

CTL_REALTIME_PRINTING = False
CTL_IGNORE_MAX_WORK_TIME = False

STAT_TOTAL_TRIES = 0

WEEKDAY_START = 6	# Sunday
WEEKDAYS = 7

MAX_WEEK_WORK_TIME = 88
WORK_TIME_START_DAY = 0

WEEKDAY_NAMES = ["Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat.", "Sun."]

# constants
class DAYTYPE(object):
	NORMAL = 0
	SP1 = 1
	SP2 = 2

MAX_MONTH_DAY = 31

class WEEKDAY(object):
	MON = 0
	TUE = 1
	WED = 2
	THU = 3
	FRI = 4
	SAT = 5
	SUN = 6

NORMAL_WORK_TIME = [8, 0, 0]
DUTY_WORK_TIME = [16, 24, 24]

class WORKDAY(object):
	def __init__(self):
		self.data = []
		self.date = datetime.date(1970, 1, 1)
		self.day_type = DAYTYPE.NORMAL
		self.possible_users = 0
		self.uid = None
class USER(object):
	def __init__(self):
		self.data = []
		self.name = ""
		self.count = [0, 0, 0]
		self.week_work_time = 0

	def __copy__(self):
		cls = self.__class__
		result = cls.__new__(cls)
		result.__dict__.update(self.__dict__)
		return result

	def __deepcopy__(self, memo):
		cls = self.__class__
		result = cls.__new__(cls)
		memo[id(self)] = result
		for k, v in self.__dict__.items():
			setattr(result, k, copy.deepcopy(v, memo))
		return result

bit_to_indexes_mapping = []

def generate_data(year, month, schedule, users, exception, day_limitation, user_limitation):
	print "Generate data"

	# generate normal calendar
	for day in range(1, MAX_MONTH_DAY + 1):
		date = datetime.date(1970, 1, 1)
		try:
			date = datetime.date(year, month, day)
		except ValueError, e:
			continue

		d = WORKDAY()
		d.date = date
		if WEEKDAY.SAT > d.date.weekday():
			d.day_type = DAYTYPE.NORMAL
		elif WEEKDAY.SUN > d.date.weekday():
			d.day_type = DAYTYPE.SP1
		else:
			d.day_type = DAYTYPE.SP2
		schedule.append(d)

	# fix calendar
	for ex in exception:
		# our calendar is zero-based indexed.
		schedule[ex[0]-1].day_type = ex[1]

	# fill possible users in each day
	for d in schedule:
		for u in users:
			if u.count[d.day_type] > 0:
				d.possible_users |= 1 << users.index(u)

	for limit in day_limitation:
		# at least one guy for this day
		if len(limit) > 1:
			day = limit.pop(0)
			schedule[day - 1].possible_users = 0
			while len(limit) > 0:
				schedule[day - 1].possible_users |= (1 << (limit.pop() - 1))

	for limit in user_limitation:
		print limit
		# 1st item is user index
		if len(limit) > 1:
			uid = limit.pop(0) - 1	# uid in config starts from 1
			while len(limit) > 0:
				day = limit.pop()
				schedule[day - 1].possible_users &= ~(1 << uid)

	for i in range(0, 2**len(users)):
		bit_to_indexes_mapping.append(bit_to_indexes(i))

def clear_screen():
	if os.name == "posix":
		os.system("clear")
	elif os.name == "nt":
		os.system("cls")

def bit_to_indexes(x):
	indexes = []
	i = 0
	while bin(x).count("1") > 0:
		if x & 0x1 > 0:
			indexes.append(i)
		i += 1
		x >>= 1
	return indexes

def bit_to_indexes_premap(x):
	return bit_to_indexes_mapping[x]

def update_user_week_work_time(day, users, duty_uid):
	for u in users:
		if day.date.weekday() == WORK_TIME_START_DAY:
			u.week_work_time = 0
		u.week_work_time += NORMAL_WORK_TIME[day.day_type]
	users[duty_uid].week_work_time += DUTY_WORK_TIME[day.day_type]

def restore_user_week_work_time(users, backup):
	for uid in range(0, len(users)):
		users[uid].week_work_time = backup[uid]

def run_schedule(schedule, users, current):
	global STAT_TOTAL_TRIES

	if current >= len(schedule):
		return True

	if schedule[current].possible_users == 0:
		STAT_TOTAL_TRIES += 1
		return False

	for uid in bit_to_indexes_premap(schedule[current].possible_users):

		if users[uid].count[schedule[current].day_type] == 0:
			continue

		# check over work time
		if not CTL_IGNORE_MAX_WORK_TIME and schedule[current].date.weekday() != WORK_TIME_START_DAY:
			estimate_week_work_time = users[uid].week_work_time +\
				NORMAL_WORK_TIME[schedule[current].day_type] +\
				DUTY_WORK_TIME[schedule[current].day_type]
			if estimate_week_work_time > MAX_WEEK_WORK_TIME:
				continue

		schedule[current].uid = uid

		if CTL_REALTIME_PRINTING:
			clear_screen()
			dump_schedule_user(schedule, users)


		users[uid].count[schedule[current].day_type] -= 1

		# remove adjacent 2 days, trick: overwrite first 2 days to avoid boundary check.
		day_index = (current + 1) % len(schedule)
		backup1 = (day_index, schedule[day_index].possible_users)
		schedule[day_index].possible_users &= ~(1 << uid)

		day_index = (current + 2) % len(schedule)
		backup2 = (day_index, schedule[day_index].possible_users)
		schedule[day_index].possible_users &= ~(1 << uid)

		# backup user week work time
		user_work_time_backup = map(lambda u:u.week_work_time, users)

		# update user week work time
		update_user_week_work_time(schedule[current], users, uid)

		if run_schedule(schedule, users, current + 1):
				return True

		schedule[backup1[0]].possible_users = backup1[1]
		schedule[backup2[0]].possible_users = backup2[1]

		# restore user week work time
		restore_user_week_work_time(users, user_work_time_backup)

		users[uid].count[schedule[current].day_type] += 1
		schedule[current].uid = None

	STAT_TOTAL_TRIES += 1
	return False

def dump_users(users):
	print "Users:"

	for u in users:
		print u.name,
		print u.count,
		print "(%d) [%d]" % (sum(u.count), u.week_work_time)

def skip_days_in_calendar(weekday_start, weekday):
	return (weekday - weekday_start) % WEEKDAYS

def dump_schedule(schedule, users):
	first = True

	# Print header
	print "%4d %02d Duty Schedule (%d days)" % (schedule[0].date.year, schedule[0].date.month, len(schedule))
	for i in range(0, WEEKDAYS):
		print "  %4s  " % (WEEKDAY_NAMES[((i + WEEKDAY_START) % WEEKDAYS)]),
	print "\n",

	for d in schedule:

		if first:
			for i in range(0, skip_days_in_calendar(WEEKDAY_START, d.date.weekday())):
				print "%s" % " "*8,

		print "(%02d, %02d)" % (d.date.day, d.day_type),

		if d.date.weekday() ==((6 + WEEKDAY_START) % WEEKDAYS):
			print "\n",
		first = False
	print "\n",

def dump_schedule_user(schedule, users, start_day=WEEKDAY_START, print_work_time=False):
	first = True

	users_for_process = copy.deepcopy(users)

	# Print header
	print "%4d %02d Duty Schedule (%d days)" % (schedule[0].date.year, schedule[0].date.month, len(schedule))
	for i in range(0, WEEKDAYS):
		print "  %4s  " % (WEEKDAY_NAMES[((i + start_day) % WEEKDAYS)]),
	print "\n",

	for d in schedule:

		if first:
			for i in range(0, skip_days_in_calendar(start_day, d.date.weekday())):
				print "%s" % " "*8,

		if d.uid >= 0:
			print "(%02d, %2s)" % (d.date.day, users_for_process[d.uid].name),
			for u in users_for_process:
				if d.date.weekday() == WORK_TIME_START_DAY:
					u.week_work_time = 0
				u.week_work_time += NORMAL_WORK_TIME[d.day_type]
			users_for_process[d.uid].week_work_time += DUTY_WORK_TIME[d.day_type]
		else:
			print "(%02d, %2d)" % (d.date.day, bin(d.possible_users_for_process).count("1")),

		if d.date.weekday() ==((6 + start_day) % WEEKDAYS):
			if print_work_time:
				print "work time:",
				for u in users_for_process:
					print " %s(%2d)" % (u.name, u.week_work_time),
			print "\n",
		first = False

	remaining_days = WEEKDAYS - (skip_days_in_calendar(start_day, (schedule[-1].date.weekday() + 1) % WEEKDAYS))
	if remaining_days < WEEKDAYS:
		for i in range(0, remaining_days):
			print "        ",
	if print_work_time:
		print "work time:",
		for u in users_for_process:
			print " %s(%2d)" % (u.name, u.week_work_time),
	print "\n",

def export_schedule_csv(path, schedule, user):

	if len(schedule) == 0:
		return

	weekday_str = ["一", "二", "三", "四", "五", "六", "日"]
	f = open(path, "wb");
	# set UTF8 BOM
	f.write(codecs.BOM_UTF8);

	f.write("ID")
	for d in schedule:
		f.write(",%02d %s" % (d.date.day, weekday_str[d.date.weekday()]))
	f.write("\r\n")

	for i in range(0, len(user)):
		f.write("%s" % user[i].name)
		for d in schedule:
			if d.uid == i:
				f.write(",R")
			else:
				if d.day_type == DAYTYPE.NORMAL:
					f.write(",A")
				else:
					f.write(",OFF")
		f.write("\r\n")

	f.close()

def export_schedule(path, schedule, users):

	if len(schedule) == 0:
		return

	day_type_colors = ["black", "#FF9900", "red"]
	name_colors = ["#006600", "#0066FF", "#6600FF", "#FF0000", "#FF9900", "#FFFF00"]
	weekday_str = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

	title = "%04d/%02d Duty Schedule" % (schedule[0].date.year, schedule[0].date.month)
	header = "%04d/%02d Duty Schedule (%d days)" % (schedule[0].date.year, schedule[0].date.month, len(schedule))
	footer = "Generated at %s" % datetime.datetime.now()

	page = markup.page()
	page.init(title=title)

	page.h1(header)

	page.table(width="100%", border=1, style="border-spacing:0px")

	first = True

	# Print header
	page.tr(style="background:#787878; color:white; font-size:26px")
	for i in range(0, WEEKDAYS):
		page.th("%s" % weekday_str[(((i + WEEKDAY_START) % WEEKDAYS))])
		page.th.close()
	page.tr.close()

	for d in schedule:

		if first or d.date.weekday() == WEEKDAY_START:
			page.tr(style="text-align:center; font-size:24px")

		if first:
			for i in range(0, skip_days_in_calendar(WEEKDAY_START, d.date.weekday())):
				page.td("-")
				page.td.close()

		if d.uid >= 0:
			page.td.open()
			page.span('%d' % d.date.day, style="color:%s" % day_type_colors[d.day_type])
			page.br()
			page.span("%s" % users[d.uid].name, style="color:%s" % name_colors[d.uid % len(name_colors)])
			page.span.close()
			page.td.close()

		if d.date.weekday() ==((6 + WEEKDAY_START) % WEEKDAYS):
			page.tr.close()

		first = False

	remaining_days = WEEKDAYS - (skip_days_in_calendar(WEEKDAY_START, (schedule[-1].date.weekday() + 1) % WEEKDAYS))
	if remaining_days < WEEKDAYS:
		for i in range(0, remaining_days):
			page.td("-")
	page.td.close()

	page.table.close()

	page.h4(footer)

	f = open(path, "w");
	f.write(str(page))
	f.close()

def dump_schedule_list(schedule, users):
	print "Schedule List (%d days)" % len(schedule)
	for d in schedule:
		print "%s (%d):" % (d.date, d.date.weekday()),
		for uid in bit_to_indexes_premap(d.possible_users):
			print "%d " % uid,
		if d.uid != None:
			print "(%s)" % users[d.uid].name,
		print "\n",

def validate_data(schedule, users):

	# check total duty day
	all_duty = sum([sum(u.count) for u in users])
	if len(schedule) != all_duty:
		print "ERROR: %d duty days,  %d is filled" % (len(schedule), all_duty)
		return False

	# check saturday
	sp1_duty = sum([(u.count[DAYTYPE.SP1]) for u in users])
	sp1_schedule = [(d.day_type == DAYTYPE.SP1) for d in schedule].count(True)
	if sp1_schedule != sp1_duty:
		print "ERROR: %d sp1 duty days, %d is filled" % (sp1_schedule, sp1_duty)
		return False

	# check sunday
	sp2_duty = sum([(u.count[DAYTYPE.SP2]) for u in users])
	sp2_schedule = [(d.day_type == DAYTYPE.SP2) for d in schedule].count(True)
	if sp2_schedule != sp2_duty:
		print "ERROR: %d sp2 duty days, %d is filled" % (sp2_schedule, sp2_duty)
		return False

	return True

def sanitize_input(in_str):
	out_str = in_str.strip()

	skip_start = out_str.find("#")
	if skip_start >= 0:
		out_str = out_str[:skip_start]

	return out_str

def read_from_file(path):
	f = open(path, "r")

	year, month = [0, 0]
	new_users = []
	duty_exception = []
	day_limitation = []
	user_limitation = []

	index = 0
	for line in f:
		line = sanitize_input(line)
		if len(line) == 0:
			continue

		# year, month
		if line.startswith("="):
			line = line[1:].strip()
			year, month = [int(x) for x in line.split()]
		# set exception for some holiday
		elif line.startswith("+"):
			line = line[1:].strip()
			duty_exception.append([int(x) for x in line.split()])
		# add user data
		elif line.startswith("@"):
			line = line[1:].strip()
			u = USER()
			u.name = chr(ord('A') + index)
			u.count = [int(x) for x in line.split()]
			new_users.append(u)
			index += 1
		elif line.startswith("!"):
			line = line[1:].strip()
			day_limitation.append([int(x) for x in line.split()])
		elif line.startswith("-"):
			line = line[1:].strip()
			user_limitation.append([int(x) for x in line.split()])
		elif line.startswith("("):
			line = line[1:].strip()
			work_time_last_month = [int(x) for x in line.split()]

			if len(work_time_last_month) == len(new_users):
				uid = 0
				for t in work_time_last_month:
					new_users[uid].week_work_time = t
					uid += 1
			else:
				print "ERROR: work time field number does not match user number."
		# skip other invalid prefix
		else:
			print "WARN: invalid prefix `%s'" % line

	f.close()

	return [year, month, new_users, duty_exception, day_limitation, user_limitation]

def usage(argv):
	print "Usage: python %s [OPTION] FILE" % argv[0]
	print ""
	print " -h, --help        print this help"
	print " -r                show realtime printing"
	print ""

def main(argv):

	opts = []
	args = []
	try:
		opts, args = getopt.getopt(argv[1:], "rht", ["help"]);
		for opt, arg in opts:
			if opt in ("-h", "--help"):
				usage(argv)
				sys.exit(1)
			if opt in ("-r"):
				CTL_REALTIME_PRINTING = True
			if opt in ("-t"):
				CTL_IGNORE_MAX_WORK_TIME = True
	except getopt.GetoptError:
		print "getopt error!"
		usage(argv)
		sys.exit(1)

	if len(args) > 0:
		path = args[0]
	else:
		usage(argv)
		sys.exit(1)

	schedule = []
	users  = []

	year, month, users, exception, day_limitation, user_limitation = read_from_file(path)

	generate_data(year, month, schedule, users, exception, day_limitation, user_limitation)
	dump_users(users)
	dump_schedule(schedule, users)

	if validate_data(schedule, users):
		users_for_process = copy.deepcopy(users)

		if run_schedule(schedule, users_for_process, 0):
			dump_users(users_for_process)
			dump_schedule_user(schedule, users)
			dump_schedule_user(schedule, users, start_day=WORK_TIME_START_DAY, print_work_time=True)
			export_schedule("%s.html" % path, schedule, users)
			export_schedule_csv("%s.csv" % path, schedule, users)
		else:
			print "ERROR: Cannot fit users to the schedule"

		print "Total tries: %d" % STAT_TOTAL_TRIES
	else:
		print "ERROR: Data are not valid"

if __name__  == '__main__':

	if len(sys.argv) < 2:
		usage(sys.argv)
		sys.exit(1)
	main(sys.argv)

